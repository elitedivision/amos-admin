<?php

/**
 * Emilia Romagna Open Innovation
 * Arter
 *
 *
 * @package    arter\amos\admin
 * @category   CategoryName
 */

namespace arter\amos\admin;

use mdm\admin\Module;
use Yii;
use yii\base\Exception;
use yii\web\ForbiddenHttpException;

/**
 * Class AmosAdmin
 * @package arter\amos\admin
 */
class RoleManager extends Module
{
    public $controllerNamespace = "mdm\admin\controllers";
    public function init()
    {
        //Views on the original plugin
        $this->viewPath = '@vendor/mdmsoft/yii2-admin/views';

        if(!Yii::$app->user->can('ADMIN')) {
            throw new ForbiddenHttpException(Yii::t('amosadmin', 'Access Denied'));
        }

        return parent::init();
    }
}
