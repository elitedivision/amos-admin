<?php

/**
 * Emilia Romagna Open Innovation
 * Arter
 *
 *
 * @package    arter\amos\admin\migrations
 * @category   CategoryName
 */

use arter\amos\admin\rules\ShowCommunityManagerWidgetRule;
use arter\amos\core\migration\AmosMigrationPermissions;

/**
 * Class m170710_082659_show_admin_community_manager_widget_rule
 */
class m170710_082659_show_admin_community_manager_widget_rule extends AmosMigrationPermissions
{
    /**
     * @inheritdoc
     */
    protected function setRBACConfigurations()
    {
        return [
            [
                'name' => \arter\amos\admin\widgets\icons\WidgetIconCommunityManagerUserProfiles::className(),
                'update' => true,
                'newValues' => [
                    'ruleName' => ShowCommunityManagerWidgetRule::className()
                ],
                'oldValues' => [
                    'ruleName' => null
                ]
            ]
        ];
    }
}
