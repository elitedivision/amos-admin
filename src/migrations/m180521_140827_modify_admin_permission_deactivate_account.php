<?php

/**
 * Emilia Romagna Open Innovation
 * Arter
 *
 *
 * @package    arter\amos\admin\migrations
 * @category   CategoryName
 */

use arter\amos\admin\rules\DeactivateAccountRule;
use arter\amos\core\migration\AmosMigrationPermissions;

/**
 * Class m180521_140827_modify_admin_permission_deactivate_account
 */
class m180521_140827_modify_admin_permission_deactivate_account extends AmosMigrationPermissions
{
    /**
     * @inheritdoc
     */
    protected function setRBACConfigurations()
    {
        return [
            [
                'name' => 'DeactivateAccount',
                'update' => true,
                'newValues' => [
                    'ruleName' => DeactivateAccountRule::className()
                ],
                'oldValues' => [
                    'ruleName' => null
                ]
            ]
        ];
    }
}
