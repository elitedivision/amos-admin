<?php

/**
 * Emilia Romagna Open Innovation
 * Arter
 *
 *
 * @package    arter\amos\admin\migrations
 * @category   CategoryName
 */

use arter\amos\core\migration\AmosMigrationPermissions;
use yii\rbac\Permission;

/**
 * Class m180611_115155_userprofile_notvalidated_workflow_status_permissions
 */
class m180611_115155_userprofile_notvalidated_workflow_status_permissions extends AmosMigrationPermissions
{
    const WORKFLOW_NAME = 'UserProfileWorkflow';
    
    /**
     * @inheritdoc
     */
    protected function setRBACConfigurations()
    {
        return [
            [
                'name' => self::WORKFLOW_NAME . '/NOTVALIDATED',
                'type' => Permission::TYPE_PERMISSION,
                'description' => 'UserProfileWorkflow status permission: Not Validated',
                'parent' => ['ADMIN', 'FACILITATOR']
            ]
        ];
    }
}
