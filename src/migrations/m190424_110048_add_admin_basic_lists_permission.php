<?php

/**
 * Emilia Romagna Open Innovation
 * Arter
 *
 *
 * @package    arter\amos\admin\migrations
 * @category   CategoryName
 */

use arter\amos\core\migration\AmosMigrationPermissions;
use yii\rbac\Permission;

/**
 * Class m190424_110048_add_admin_basic_lists_permission
 */
class m190424_110048_add_admin_basic_lists_permission extends AmosMigrationPermissions
{
    /**
     * @inheritdoc
     */
    protected function setRBACConfigurations()
    {
        return [
            [
                'name' => 'USER_PROFILE_BASIC_LIST_ACTIONS',
                'type' => Permission::TYPE_PERMISSION,
                'description' => 'Permission to see the basic admin plugin lists',
                'parent' => ['AMMINISTRATORE_UTENTI', 'BASIC_USER']
            ]
        ];
    }
}
