<?php

/**
 * Emilia Romagna Open Innovation
 * Arter
 *
 *
 * @package    arter\amos\admin\migrations
 * @category   CategoryName
 */

use arter\amos\core\migration\AmosMigrationPermissions;
use yii\rbac\Permission;

/**
 * Class m170623_133028_add_admin_facilitator_role
 */
class m170623_133028_add_admin_facilitator_role extends AmosMigrationPermissions
{
    /**
     * @inheritdoc
     */
    protected function setRBACConfigurations()
    {
        return [
            [
                'name' => 'FACILITATOR',
                'type' => Permission::TYPE_ROLE,
                'description' => 'Facilitator platform role'
            ]
        ];
    }
}
