<?php

/**
 * Emilia Romagna Open Innovation
 * Arter
 *
 *
 * @package    arter\amos\admin\migrations
 * @category   CategoryName
 */

use arter\amos\core\migration\AmosMigrationTableCreation;

/**
 * Handles the creation of table `user_profile_area_mm` and all accessory changes.
 */
class m230724_010334_set_user_profile_area_as_mm extends AmosMigrationTableCreation
{

    /**
     * @inheritdoc
     */
    protected function setTableName()
    {
        $this->tableName = '{{%user_profile_area_mm}}';
    }

    /**
     * @inheritdoc
     */
    protected function setTableFields()
    {
        $this->tableFields = [
            'id' => $this->primaryKey(),
            'user_profile_id' => $this->integer()->notNull()->comment('User Profile ID'),
            'user_profile_area_id' => $this->integer()->notNull()->comment('User Profile Area ID'),
            'order' => $this->integer()->null()->comment('Order'),
        ];
    }

    /**
     * @inheritdoc
     */
    protected function beforeTableCreation()
    {
        parent::beforeTableCreation();
        $this->setAddCreatedUpdatedFields(true);
    }

    /**
     * @inheritdoc
     */
    protected function addForeignKeys()
    {
        $this->addForeignKey('fk_user_profile_area_mm_user_profile', $this->tableName, 'user_profile_id', 'user_profile', 'id');
        $this->addForeignKey('fk_user_profile_area_mm_user_profile_area', $this->tableName, 'user_profile_area_id', 'user_profile_area', 'id');
    }

    /**
     * @inheritdoc
     */
    protected function afterTableCreation() {

        $modelClass = \arter\amos\admin\AmosAdmin::instance()->createModel('UserProfile');
        $profiles = $modelClass::find()->all();

        foreach($profiles as $profile) {
            if (!empty($profile->user_profile_area_id))
                $this->insert($this->tableName, ['user_profile_id' => $profile->id, 'user_profile_area_id' => $profile->user_profile_area_id]);
        }
        $this->dropForeignKey('fk_user_profile_area_user_profile', 'user_profile');
        $this->dropColumn('user_profile', 'user_profile_area_id');

        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn('user_profile','user_profile_area_id', 'integer');
        $this->addForeignKey('fk_user_profile_area_user_profile', 'user_profile', 'user_profile_area_id', 'user_profile_area', 'id');
        $mms = \arter\amos\admin\models\UserProfileAreaMm::find()->all();

        foreach($mms as $mm) {
            $this->update('user_profile', ['user_profile_area_id' => $mm->user_profile_area_id], ['id' => $mm->user_profile_id]);
        }

        $this->dropTable($this->tableName);

        return true;
    }
}
