<?php

/**
 * Emilia Romagna Open Innovation
 * Arter
 *
 *
 * @package    arter\amos\admin\views\security
 * @category   CategoryName
 */

/**
 * @var yii\web\View $this
 * @var string $message
 */

?>

<div class="<?= Yii::$app->controller->id ?>-unsubscribe col-xs-12">
    <div class="text-center">
        <?= $message ?>
    </div>
</div>
