<?php

/**
 * Emilia Romagna Open Innovation
 * Arter
 *
 *
 * @package    arter\amos\admin\views\user-profile\boxes
 * @category   CategoryName
 */

use arter\amos\admin\AmosAdmin;
use arter\amos\core\icons\AmosIcons;
use arter\amos\notificationmanager\widgets\NotifyFrequencyWidget;

/**
 * @var yii\web\View $this
 * @var arter\amos\core\forms\ActiveForm $form
 * @var arter\amos\admin\models\UserProfile $model
 * @var arter\amos\core\user\User $user
 */

/** @var AmosAdmin $adminModule */
$adminModule = Yii::$app->controller->module;

?>
    <h2><?= AmosAdmin::t('amosadmin', 'Configure notifications') . '.' ?></h2>
    <p><?= AmosAdmin::t('amosadmin', 'If the frequency is not indicated, you will receive the notifications as automatically set by the system') . '.' ?></p>
    <div class="col-xs-12 nop m-t-15">
        <?= \arter\amos\notificationmanager\widgets\NotifyFrequencyAdvancedWidget::widget([
            'model' => $model
        ]) ?>
    </div>

