<?php

/**
 * Emilia Romagna Open Innovation
 * Arter
 *
 *
 * @package    arter\amos\admin\views\user-profile\boxes
 * @category   CategoryName
 */

use arter\amos\admin\AmosAdmin;
use arter\amos\admin\base\ConfigurationManager;
use arter\amos\attachments\components\CropInput;

/**
 * @var yii\web\View $this
 * @var arter\amos\core\forms\ActiveForm $form
 * @var arter\amos\admin\models\UserProfile $model
 * @var arter\amos\core\user\User $user
 */

/** @var AmosAdmin $adminModule */
$adminModule = Yii::$app->controller->module;

?>

<?php if ($adminModule->confManager->isVisibleField('userProfileImage', ConfigurationManager::VIEW_TYPE_FORM)): ?>
    <?= $form->field($model,'userProfileImage')->widget(CropInput::classname(), [
        'enableUploadFromGallery' => false,
        'jcropOptions' => [ 'aspectRatio' => '1']
    ])->label(AmosAdmin::t('amosadmin', '#image_field'))->hint(AmosAdmin::t('amosadmin', '#image_field_hint')); ?>
<?php endif; ?>
