<?php

/**
 * Emilia Romagna Open Innovation
 * Arter
 *
 *
 * @package    arter\amos\admin\views\user-profile\boxes
 * @category   CategoryName
 */

use arter\amos\admin\AmosAdmin;

/**
 * @var yii\web\View $this
 * @var arter\amos\core\forms\ActiveForm $form
 * @var arter\amos\admin\models\UserProfile $model
 * @var arter\amos\core\user\User $user
 */

/** @var AmosAdmin $adminModule */
$adminModule = Yii::$app->controller->module;

?>

<section class="section-data">
    <h2>
        <?= AmosAdmin::tHtml('amosadmin', 'Questio') ?>
    </h2>
    <div class="row">
        <!-- TODO creare qui la sezione di questio -->
    </div>
</section>
