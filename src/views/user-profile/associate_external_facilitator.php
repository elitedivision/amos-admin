<?php
use yii\helpers\Html;
use arter\amos\core\module\BaseAmosModule;
use arter\amos\core\icons\AmosIcons;


echo $this->render('_search_external_facilitator', ['model' => $model]);

echo \arter\amos\core\views\ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '@vendor/arter/amos-admin/src/views/user-profile/_item'
]);
