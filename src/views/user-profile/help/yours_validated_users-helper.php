<?php

/**
 * Emilia Romagna Open Innovation
 * Arter
 *
 *
 * @package    arter\amos\bestpratices\views\best-pratice\help
 * @category   CategoryName
 */

use arter\amos\admin\AmosAdmin;

$label = AmosAdmin::t('amosadmin', '#yours_validated_users');

if(!empty($label)) : ?>
    <div class="yours-validated-users-description">
        <?= $label ?>
    </div>
<?php endif; ?>
