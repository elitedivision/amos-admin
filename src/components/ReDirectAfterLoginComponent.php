<?php

/**
 * Emilia Romagna Open Innovation
 * Arter
 *
 *
 * @package    arter\amos\admin\components
 * @category   CategoryName
 */

namespace arter\amos\admin\components;

use arter\amos\admin\AmosAdmin;
use arter\amos\admin\models\UserProfile;
//use yii\base\BootstrapInterface;
use yii\base\Component;
//use yii\base\Event;

/**
 * Class FirstAccessWizardComponent
 * @package arter\amos\admin\components
 */
class ReDirectAfterLoginComponent extends Component /*implements BootstrapInterface*/
{

    /**
     * @param $url
     * @return \yii\web\Response
     */
    public function redirectToUrl($url){
        return \Yii::$app->controller->redirect([$url]);

    }
}
