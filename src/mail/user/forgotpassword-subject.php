<?php

/**
 * Emilia Romagna Open Innovation
 * Arter
 *
 *
 * @package    arter\amos\admin\mail\user
 * @category   CategoryName
 */

use arter\amos\admin\AmosAdmin;
use arter\amos\admin\utility\UserProfileUtility;

/**
 * @var \arter\amos\admin\models\UserProfile $profile
 */

?>

<?= AmosAdmin::t('amosadmin', '#reset_password_for') . ' ' . \Yii::$app->name ?>
