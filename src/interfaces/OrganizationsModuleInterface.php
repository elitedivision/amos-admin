<?php

/**
 * Emilia Romagna Open Innovation
 * Arter
 *
 *
 * @package    arter\amos\admin\interfaces
 * @category   CategoryName
 */

namespace arter\amos\admin\interfaces;

/**
 * @deprecated Use \arter\amos\core\interfaces\OrganizationsModuleInterface
 *
 * Interface OrganizationsModuleInterface
 * @package arter\amos\admin\interfaces
 */
interface OrganizationsModuleInterface extends \arter\amos\core\interfaces\OrganizationsModuleInterface
{

}