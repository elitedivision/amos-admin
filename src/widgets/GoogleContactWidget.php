<?php
/**
 * Emilia Romagna Open Innovation
 * Arter
 *
 *
 * @package    arter\amos\admin\widgets
 * @category   CategoryName
 */

namespace arter\amos\admin\widgets;

use arter\amos\admin\AmosAdmin;
use arter\amos\admin\models\UserProfile;
use arter\amos\core\icons\AmosIcons;
use yii\base\Widget;

/**
 * Class GoogleContactWidget
 * @package arter\amos\admin\widgets
 */
class GoogleContactWidget extends Widget
{
    /** @var  UserProfile $model */
    public $model;

    /**
     * widget initialization
     */
    public function init()
    {
        parent::init();

        if (is_null($this->model)) {
            throw new \Exception(AmosAdmin::t('amosadmin', 'Missing model'));
        }
    }

    /**
     * @inheritdoc
     */
    public function run()
    {

        if($this->model->isGoogleContact()) {
            $googleContactIcon = AmosIcons::show('google', [
                'class' => 'am-2',
                'title' => AmosAdmin::t('amosadmin', 'Google Contact')
            ]);
            return $googleContactIcon;
        }else{
            return '';
        }
    }


}