<?php

/**
 * Emilia Romagna Open Innovation
 * Arter
 *
 *
 * @package    arter\amos\admin\events
 * @category   CategoryName
 */

namespace arter\amos\admin\events;

use yii\base\Event;

interface AdminWorkflowEventInterface
{
    /**
     * This method assign CREATOR_... roles to users with status ATTIVOEVALIDATO
     *
     * @param Event $event
     */
    public function assignCreatorRoles(Event $event);
}
