<?php

/**
 * Emilia Romagna Open Innovation
 * Arter
 *
 *
 * @package    arter\amos\admin\rules
 * @category   CategoryName
 */

namespace arter\amos\admin\rules;

use arter\amos\admin\models\UserProfile;
use arter\amos\core\rules\BasicContentRule;

/**
 * Class DeactivateAccountRule
 * @package arter\amos\admin\rules
 */
class DeactivateAccountRule extends BasicContentRule
{
    public $name = 'deactivateAccount';

    /**
     * @inheritdoc
     */
    public function ruleLogic($user, $item, $params, $model)
    {
        if (!\Yii::$app->user->can('ADMIN') && !\Yii::$app->user->can('AMMINISTRATORE_UTENTI')) {
            return true;
        }
        if (is_null($model) || (!($model instanceof UserProfile))) {
            return false;
        }
        return ($user != $model->user_id);
    }
}
