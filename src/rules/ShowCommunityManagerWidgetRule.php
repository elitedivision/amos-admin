<?php

/**
 * Emilia Romagna Open Innovation
 * Arter
 *
 *
 * @package    arter\amos\admin\rules
 * @category   CategoryName
 */

namespace arter\amos\admin\rules;

use yii\rbac\Rule;

/**
 * Class ShowCommunityManagerWidgetRule
 * @package arter\amos\admin\rules
 */
class ShowCommunityManagerWidgetRule extends Rule
{
    public $name = 'showCommunityManagerWidget';
    
    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        $communityModule = \Yii::$app->getModule('community');
        return (!is_null($communityModule));
    }
}
