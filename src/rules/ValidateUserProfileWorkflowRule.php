<?php

/**
 * Emilia Romagna Open Innovation
 * Arter
 *
 *
 * @package    arter\amos\admin\rules
 * @category   CategoryName
 */

namespace arter\amos\admin\rules;

use arter\amos\admin\models\UserProfile;
use arter\amos\core\rules\BasicContentRule;

/**
 * Class UpdateProfileFacilitatorRule
 * @package arter\amos\admin\rules
 */
class ValidateUserProfileWorkflowRule extends BasicContentRule
{
    public $name = 'ValidateUserProfileWorkflow';
    
    /**
     * Facilitator cannot validate his own profile
     * @inheritdoc
     */
    public function ruleLogic($user, $item, $params, $model)
    {
        // Return false if the model is null
        if (is_null($model)) {
            return false;
        }

        /** @var UserProfile $model */
        if(\Yii::$app->user->can('FACILITATOR')){
            if($model->user_id == $user)
                return false;
            else return true;
        }
        return false;
    }
}
