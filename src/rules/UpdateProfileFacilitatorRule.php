<?php

/**
 * Emilia Romagna Open Innovation
 * Arter
 *
 *
 * @package    arter\amos\admin\rules
 * @category   CategoryName
 */

namespace arter\amos\admin\rules;

use arter\amos\admin\models\UserProfile;
use arter\amos\core\rules\BasicContentRule;

/**
 * Class UpdateProfileFacilitatorRule
 * @package arter\amos\admin\rules
 */
class UpdateProfileFacilitatorRule extends BasicContentRule
{
    public $name = 'updateProfileFacilitator';
    
    /**
     * @inheritdoc
     */
    public function ruleLogic($user, $item, $params, $model)
    {
        // Return false if the model is null
        if (is_null($model)) {
            return false;
        }
        /** @var UserProfile $model */
        
        // Check if the profile has a facilitator
        if (is_null($model->facilitatore)) {
            return false;
        }
        
        // Check if the profile facilitator is the logged user
        return ($model->facilitatore->user_id == $user);
    }
}
