<?php

/**
 * Emilia Romagna Open Innovation
 * Arter
 *
 *
 * @package    arter\amos\admin\models
 * @category   CategoryName
 */

namespace arter\amos\admin\models;

/**
 * Class UserProfileAgeGroup
 * This is the model class for table "user_profile_age_group".
 * @package arter\amos\admin\models
 */
class UserProfileAgeGroup extends \arter\amos\admin\models\base\UserProfileAgeGroup
{

}
