<?php

/**
 * Emilia Romagna Open Innovation
 * Arter
 *
 *
 * @package    arter\amos\admin\models
 * @category   CategoryName
 */

namespace arter\amos\admin\models;

/**
 * Class UserProfileArea
 * This is the model class for table "user_profile_area".
 * @package arter\amos\admin\models
 */
class UserProfileArea extends \arter\amos\admin\models\base\UserProfileArea
{

}
