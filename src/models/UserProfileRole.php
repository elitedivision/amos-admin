<?php

/**
 * Emilia Romagna Open Innovation
 * Arter
 *
 *
 * @package    arter\amos\admin\models
 * @category   CategoryName
 */

namespace arter\amos\admin\models;

/**
 * Class UserProfileRole
 * This is the model class for table "user_profile_role".
 * @package arter\amos\admin\models
 */
class UserProfileRole extends \arter\amos\admin\models\base\UserProfileRole
{

}
