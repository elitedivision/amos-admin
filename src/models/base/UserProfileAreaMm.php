<?php

/**
 * Emilia Romagna Open Innovation
 * Arter
 *
 *
 * @package    arter\amos\admin\models\base
 * @category   CategoryName
 */

namespace arter\amos\admin\models\base;

use arter\amos\admin\AmosAdmin;
use arter\amos\core\record\Record;

/**
 * Class UserProfileArea
 * This is the base-model class for table "user_profile_area_mm".
 *
 * @property integer $id
 * @property integer $user_profile_id
 * @property integer $user_profile_area_id
 * @property integer $order
 * *
 * @package arter\amos\admin\models\base
 */
class UserProfileAreaMm extends Record
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_profile_area_mm';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_profile_id', 'user_profile_area_id', 'order'], 'safe'],
            [['user_profile_id', 'user_profile_area_id'], 'required'],
            [['user_profile_id', 'user_profile_area_id', 'order'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => AmosAdmin::t('amosadmin', 'ID'),
            'user_profile_id' => AmosAdmin::t('amosadmin', 'User Profile ID'),
            'user_profile_area_id' => AmosAdmin::t('amosadmin', 'User Profile Area'),
            'order' => AmosAdmin::t('amosadmin', 'Order'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfile()
    {
        $modelClass = \arter\amos\admin\AmosAdmin::instance()->createModel('UserProfile');
        return ($modelClass::className())::findOne($this->user_profile_id);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfileArea()
    {
        return (\arter\amos\admin\models\UserProfileArea::className())::findOne($this->user_profile_area_id);
    }
}
