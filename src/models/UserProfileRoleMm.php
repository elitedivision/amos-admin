<?php
/**
 * Emilia Romagna Open Innovation
 * Arter
 *
 *
 * @package    arter\amos\admin\models
 * @category   CategoryName
 */

namespace arter\amos\admin\models;


/**
 * Class UserProfileAreaMm
 *
 * This is the model class for table "user_profile_area_mm".
 * @package arter\amos\admin\models
 */
class UserProfileAreaMm extends \arter\amos\admin\models\base\UserProfileAreaMm
{
}
