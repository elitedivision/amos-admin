<?php

/**
 * Emilia Romagna Open Innovation
 * Arter
 *
 *
 * @package    arter\amos\admin\config
 * @category   CategoryName
 */

return [
    'components' => [
        // List of component configurations
        'formatter' => [
            'class' => 'arter\amos\core\formatter\Formatter',
        ],
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptchaConfig',
        ],
    ],
    'params' => [
        // Active the search
        'searchParams' => [
            'user-profile' => [
                'enable' => true
            ]
        ],

        // Active the order
        'orderParams' => [
            'user-profile' => [
                'enable' => true,
                'fields' => [
                    'nome',
                    'cognome',
                    'surnameName',
                    'prevalentPartnership',
                    'created_at'
                ],
                'default_field' => 'surnameName',
                'order_type' => SORT_ASC
            ]
        ]
    ]
];
